import React, { Component } from 'react';
import './App.css';
import Number from './Numbers/Number';
class App extends Component {
    state = {
        newnumbers: []
};
    generateRandomNumbers = () => {
        const numbers = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36];
        let randomnumbers = [];
         for (let i = 0; i < 5; i++) {
            let randomnumbers2 = Math.floor(Math.random() * numbers.length);
            randomnumbers.push(numbers[randomnumbers2]);
             numbers.splice(randomnumbers2, 1);
            console.log(randomnumbers);
        }
        this.setState({newnumbers: randomnumbers.sort((a, b) => a > b)});
        return randomnumbers;
    };
  render() {
      // let array = [];
      // for (let i = 0; i < this.state.newnumbers.length; i++) {
      //     array.push(<Number number = {this.state.newnumbers[i]} key = {i}/>)
      // }
    return (
      <div className="App">
          <button className="btn" onClick={this.generateRandomNumbers}>New numbers</button>
          <div className="newapp">
              {this.state.newnumbers.map((number,index) => <Number number = {number} key = {index}/>)}
          </div>
      </div>
    );
  }
}

          export default App;
